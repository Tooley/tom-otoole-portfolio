// JavaScript Document
$(function(){
	var $allVideos = $("iframe[src^='http://www.youtube.com'],object, embed"), 
		$flEl = $("#videoWrapper");
	$allVideos.each(function(){
		$(this)
		.attr('data-aspectRatio', this.height / this.width)
		.removeAttr('height')
		.removeAttr('width');
	});
	$(window).resize(function() {
        var newWidth = $flEl.width();
		$allVideos.each(function() {
            var $el = $(this);
			$el
				.width(newWidth)
				.height(newWidth * $el.attr('data-aspectRatio'));
				
        });
    }).resize();
});